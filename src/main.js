// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Index from './components/Index'
import 'bootstrap' 
import router from './router/index'
import Vuex from 'vuex'
Vue.use(Vuex)
Vue.config.productionTip = false

/* eslint-disable no-new */

new Vue({
  el: '#app',
  render: h => h(App),
  router  //  router:router

        /*
        * new Vue({
        * el:'#app',
        * render: function(h){
        * h(App)
        * }
        * })
        * */


})
const store = new Vuex.Store({
  state: {
    count: 0
  },
  getters: {
    getCounter : state =>{
      return state.count
    }
  },
  mutations: {
    increment (state, number) {
      state.count+= number
    }
  },
  actions: {
    increment (context, number) {
      context.commit('increment', number)
    }
  }
})
/* eslint-disable no-new */

/*new Vue({
  el: '#index',
  router,
  components: { Index },
  template: '<Index/>'
})*/
